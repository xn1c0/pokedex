#ifndef TYPEWIDGET_H
#define TYPEWIDGET_H

#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QDebug>

/**
 * @brief The TypeWidget class
 * Special widget to put in tablewidget type column
 */
class TypeWidget : public QWidget {
    Q_OBJECT
    public:
        TypeWidget(QWidget *parent, QStringList &types);

        QLabel* getType1();
        QLabel* getType2();
        const QLabel* getType1() const;
        const QLabel* getType2() const;

    private :
        QLabel *type1;
        QLabel *type2;
};

#endif // TYPEWIDGET_H
