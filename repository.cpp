#include "repository.h"

repository* repository::_repository = nullptr;

/**
 * Static method defininition
 */
repository *repository::GetInstance()
{
    /**
     * This is a safer way to create an instance. instance = new Singleton is
     * dangeruous in case two instance threads wants to access at the same time
     */
    if(_repository == nullptr){
        _repository = new repository();
    }
    return _repository;
}

QList<QStringList> repository::getData() {
    // File reading
    QFile file(_filePath);
    if(!file.open(QFile::Text | QFile::ReadOnly))
        throw;
    QString data = file.readAll(); // Read all file
    file.flush();
    file.close();

    // rows to string list
    QStringList rowsData = data.split('\n');
    // if exists remove last '\n' string
    if(rowsData.last().isEmpty()) rowsData.pop_back();

    static QRegularExpression iqs("\\s+(?=(?:(?:[^\\\"]*\\\"){2})*[^\\\"]*\\\"[^\\\"]*$)");
    static QRegularExpression iqc(",+(?=(?:(?:[^\\\"]*\\\"){2})*[^\\\"]*\\\"[^\\\"]*$)");

    QList<QStringList> result;
    for(int i = 0; i < rowsData.size(); ++i)
        result.push_back(rowsData[i].replace(iqs, "").replace(iqc, "-").replace("\"", "").split(','));

    return result;
}
