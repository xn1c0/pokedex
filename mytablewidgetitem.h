#ifndef MYTABLEWIDGETITEM_H
#define MYTABLEWIDGETITEM_H

#include <QTableWidgetItem>
#include <QLabel>
#include <QDebug>
#include "typewidget.h"

/**
 * @brief The MyTableWidgetItem class
 * Custom item for type column
 */
class MyTableWidgetItem : public TypeWidget, public QTableWidgetItem {
    Q_OBJECT
    public:
    MyTableWidgetItem(QWidget* parent, QStringList &types);
    bool operator <(const QTableWidgetItem& other) const;
};

#endif // MYTABLEWIDGETITEM_H
