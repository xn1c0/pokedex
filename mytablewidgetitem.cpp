#include "mytablewidgetitem.h"

MyTableWidgetItem::MyTableWidgetItem(QWidget* parent, QStringList &types): TypeWidget(parent, types) {}

bool MyTableWidgetItem::operator<(const QTableWidgetItem& other) const {
    /* TypeWidget */
    if(other.column() == 3) {
        const TypeWidget *p = dynamic_cast<const TypeWidget *>(&other);
        if(p != 0) {
            if(this->getType1() != nullptr) {
                if(this->getType1()->text().compare(p->getType1()->text()) < 0)
                    return true;
            }
        }
    }
    return false;
}
