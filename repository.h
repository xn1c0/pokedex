#ifndef REPOSITORY_HPP
#define REPOSITORY_HPP

#include <QString>
#include <QStringList>
#include <QRegularExpression>
#include <QDir>
#include <QPixmap>
#include <QDebug>

/**
 * @brief The repository class
 * Used to access the csv file and adjust it
 */
class repository {
    protected:
        repository(): _filePath(QDir::currentPath().append("/pokedex.csv")) {}
        static repository* _repository;
        const QString _filePath;

    public:
        /**
         * @brief repository should not be cloneable.
         */
        repository(repository &other) = delete;
        /**
         * @brief repository should not be assignable.
         */
        void operator=(const repository &) = delete;
        /**
         * @brief This is the static method that controls the access to the repository instance
         */
        static repository *GetInstance();
        /**
         * @brief Returns a QList<QStringList>
         * Formatted csv data to make it simpler to use
         */
        QList<QStringList> getData();
};

#endif // REPOSITORY_HPP
