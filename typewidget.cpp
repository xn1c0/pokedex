#include "typewidget.h"

TypeWidget::TypeWidget(QWidget *parent, QStringList &types): QWidget{parent} {
    QVBoxLayout *layout = new QVBoxLayout(this); // this keyword is iimportant, we tell the widget the parent withn be showed
    layout->setMargin(0);

    QList<QColor> typeColor = { QColor(110, 111, 112), QColor(230, 40, 7), QColor(0, 68, 171), QColor(201, 177, 18), QColor(9, 217, 47), QColor(31, 194, 183), QColor(194, 106, 23), QColor(173, 82, 191), QColor(84, 47, 28), QColor(105, 219, 181), QColor(170, 18, 179), QColor(2, 82, 16), QColor(59, 61, 59), QColor(54, 45, 42), QColor(125, 95, 5), QColor(60, 61, 66), QColor(64, 70, 82), QColor(224, 150, 192) };
    QStringList typeList = { "Normal", "Fire", "Water", "Electric", "Grass", "Ice", "Fighting", "Poison", "Ground", "Flying", "Psychic", "Bug", "Rock", "Ghost", "Dragon", "Dark", "Steel", "Fairy" };

    type1 = new QLabel(types[0]);

    int colIndex = typeList.indexOf(types[0]);
    QVariant variant = typeColor[colIndex];
    QString colcode = variant.toString();
    type1->setMaximumHeight(15);
    type1->setStyleSheet("QLabel { background-color :"+colcode+" ; color : black; }");
    type1->setAlignment(Qt::AlignCenter);
    type1->setMargin(0);
    layout->addWidget(type1);

    if(types.size() == 2) {
        type2 = new QLabel(types[1]);
        type2->setMaximumHeight(15);
        int colIndex = typeList.indexOf(types[1]);
        QVariant variant = typeColor[colIndex];
        QString colcode = variant.toString();
        type2->setStyleSheet("QLabel { background-color :"+colcode+" ; color : black; }");
        type2->setAlignment(Qt::AlignCenter);
        type2->setMargin(0);
        layout->addWidget(type2);
    }
}

QLabel* TypeWidget::getType1() { return type1; }
QLabel* TypeWidget::getType2() { return type2; }
const QLabel* TypeWidget::getType1() const { return type1; }
const QLabel* TypeWidget::getType2() const { return type2; }
