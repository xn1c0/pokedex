#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow) {

    lastSelectionBar1Index = -1;
    lastSelectionBar2Index = -1;

    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0); // HomePage Prefix
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    /* Table Data Initialization and selectionBars*/
    loadData();

    /* Chart setup */
    series1 = new QLineSeries();
    series2 = new QLineSeries();
    chart = new QPolarChart();
    angularAxis = new QCategoryAxis();
    radialAxis = new QValueAxis();
    // aggiungi
    chart->addSeries(series1);
    chart->addSeries(series2);
    chart->addAxis(angularAxis, QPolarChart::PolarOrientationAngular);
    chart->addAxis(radialAxis, QPolarChart::PolarOrientationRadial);
    series1->attachAxis(radialAxis);
    series1->attachAxis(angularAxis);
    series2->attachAxis(radialAxis);
    series2->attachAxis(angularAxis);
    radialAxis->setTickCount(10);
    radialAxis->setLabelFormat("%d");

    /* Signals to Slot connection */
    connect(ui->navigateToHomeButton, SIGNAL(clicked()), this, SLOT(navigateToHomeButtonClicked()));
    connect(ui->clearFiltersButton, SIGNAL(clicked()), this, SLOT(clearFiltersButtonClicked()));
    connect(ui->enableFiltersButton, SIGNAL(clicked()), this, SLOT(enableFiltersButtonClicked()));
    connect(ui->selectionBar1, SIGNAL(currentIndexChanged(int)), this, SLOT(selectionBar1CurrentIndexChanged(int)));
    connect(ui->selectionBar2, SIGNAL(currentIndexChanged(int)), this, SLOT(selectionBar2CurrentIndexChanged(int)));
    connect(ui->tableWidget, SIGNAL(cellClicked(int, int)), this, SLOT(myCellClicked(int, int)));
    for (int i = 0; i < ui->filterGrid->count(); ++i) {
        QCheckBox *checkBox = static_cast<QCheckBox*>(ui->filterGrid->itemAt(i)->widget());
        checkBox->setCheckState(Qt::CheckState::Checked);
        connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(filterCBStateChanged(int)));
    }

    // after connection change index and update content
    ui->selectionBar1->setMaxVisibleItems(15);
    ui->selectionBar2->setMaxVisibleItems(15);
    ui->selectionBar1->setCurrentIndex(1);
    ui->selectionBar1->setCurrentIndex(0);
    ui->selectionBar2->setCurrentIndex(1);
}

MainWindow::~MainWindow() {
    /* chart is the parent of this data */
//    delete angularAxis;
//    delete radialAxis;
//    delete series1;
//    delete series2;
    delete chart;
    delete ui;
}

void MainWindow::myCellClicked(const int row, const int column) {
    if(column == 2) {
        int index = ui->tableWidget->item(row, 1)->text().toInt();
        if(index == ui->selectionBar2->currentIndex()) {
            ui->selectionBar1->setCurrentIndex(index);
            ui->selectionBar2->setCurrentIndex(index + 1);
        }
        else {
            ui->selectionBar1->setCurrentIndex(index);
        }
        ui->stackedWidget->setCurrentIndex(1);
    }
}

void MainWindow::selectionBar1CurrentIndexChanged(const int index) {

    QList<QTableWidgetItem *> items = ui->tableWidget->findItems(ui->selectionBar1->itemText(index), Qt::MatchExactly);
    int realIndex = items.at(0)->row();

    // selectionBar2 update
    ui->selectionBar2->setItemData(lastSelectionBar2Index, 33, Qt::UserRole - 1);
    ui->selectionBar2->setItemData(index, 0, Qt::UserRole - 1);
    lastSelectionBar2Index = index;

    // image processing, data is retrived from the table (indexes of table and combobox are equals)
    QString imagePath = ui->tableWidget->item(realIndex, 0)->text();
    QString filePath = QDir::currentPath().append("/").append(imagePath);
    ui->pokemon1Image->setPixmap(QPixmap(filePath));

    // Update QLabel on compare screen
    QObjectList list = ui->tableWidget->cellWidget(realIndex, 3)->children();
    ui->pokemon1Type1->setText(static_cast<QLabel*>(list[1])->text());
    ui->pokemon1Type1->setStyleSheet(static_cast<QLabel*>(list[1])->styleSheet());
    if(list.size() == 2)
        ui->pokemon1Type2->setHidden(true);
    else {
        ui->pokemon1Type2->setHidden(false);
        ui->pokemon1Type2->setText(static_cast<QLabel*>(list[2])->text());
        ui->pokemon1Type2->setStyleSheet(static_cast<QLabel*>(list[2])->styleSheet());
    }

    // update chart
    updateChart(ui->selectionBar1->currentIndex(), ui->selectionBar2->currentIndex());
}

void MainWindow::selectionBar2CurrentIndexChanged(const int index) {

    QList<QTableWidgetItem *> items = ui->tableWidget->findItems(ui->selectionBar1->itemText(index), Qt::MatchExactly);
    int realIndex = items.at(0)->row();

    // selectionBar1 update
    ui->selectionBar1->setItemData(lastSelectionBar1Index, 33, Qt::UserRole - 1);
    ui->selectionBar1->setItemData(index, 0, Qt::UserRole - 1);
    lastSelectionBar1Index = index;


    // image processing, data is retrived from the table (indexes of table and combobox are equals)
    QString imagePath = ui->tableWidget->item(realIndex, 0)->text();
    QString filePath = QDir::currentPath().append("/").append(imagePath);
    ui->pokemon2Image->setPixmap(QPixmap(filePath));

    // Update QLabel on compare screen
    QObjectList list = ui->tableWidget->cellWidget(realIndex, 3)->children();
    ui->pokemon2Type1->setText(static_cast<QLabel*>(list[1])->text());
    ui->pokemon2Type1->setStyleSheet(static_cast<QLabel*>(list[1])->styleSheet());
    if(list.size() == 2)
        ui->pokemon2Type2->setHidden(true);
    else {
        ui->pokemon2Type2->setHidden(false);
        ui->pokemon2Type2->setText(static_cast<QLabel*>(list[2])->text());
        ui->pokemon2Type2->setStyleSheet(static_cast<QLabel*>(list[2])->styleSheet());
    }

    // update chart
    updateChart(ui->selectionBar1->currentIndex(), ui->selectionBar2->currentIndex());
}

void MainWindow::filterTable(const QString &filter, const bool show) {
    for(int i = 0; i < ui->tableWidget->rowCount(); ++i) {
        QWidget *item = ui->tableWidget->cellWidget(i, 3);
        QObjectList list = item->children();
        for(int j = 1; j < list.size(); ++j) {
            if(static_cast<QLabel*>(list[j])->text().contains(filter))
                ui->tableWidget->setRowHidden(i, show);
        }
    }
}

void MainWindow::navigateToHomeButtonClicked() {
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::filterCBStateChanged(const int state) {
    QCheckBox* checkbox = qobject_cast<QCheckBox*>(sender());
    if (checkbox)
        filterTable(checkbox->text(), (state == Qt::CheckState::Checked) ? false : true);
}

void MainWindow::clearFiltersButtonClicked() {
    for (int i = 0; i < ui->filterGrid->count(); ++i) {
        QCheckBox *checkBox = static_cast<QCheckBox*>(ui->filterGrid->itemAt(i)->widget());
        checkBox->setCheckState(Qt::CheckState::Unchecked);
    }
}

void MainWindow::enableFiltersButtonClicked() {
    for (int i = 0; i < ui->filterGrid->count(); ++i) {
        QCheckBox *checkBox = static_cast<QCheckBox*>(ui->filterGrid->itemAt(i)->widget());
        checkBox->setCheckState(Qt::CheckState::Checked);
    }
}

void MainWindow::updateChart(const int barIndex1, const int barIndex2) {

    QList<QTableWidgetItem *> items1 = ui->tableWidget->findItems(ui->selectionBar1->itemText(barIndex1), Qt::MatchExactly);
    QList<QTableWidgetItem *> items2 = ui->tableWidget->findItems(ui->selectionBar1->itemText(barIndex2), Qt::MatchExactly);
    int realIndex1 = items1.at(0)->row();
    int realIndex2 = items2.at(0)->row();

    int max_tot = 0;

    const QVector<double> polarChartAngularAxis = { 0.0, 51.428, 102.856, 154.284, 205.712, 257.14, 308.568, 360.0 };
    const QVector<QString> polarChartLabels = { "Total", "Speed", "Sp. Def", "Sp. Atk", "Defense", "Attack", "HP" };

    series1->clear();
    series2->clear();

    angularAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);
    angularAxis->setRange(0, 360);
    for(int i = 0; i < polarChartAngularAxis.size() - 1; ++i)
        angularAxis->append(polarChartLabels[i], polarChartAngularAxis[i]);

    for(int i = 0, col = 4; i < polarChartAngularAxis.size(); ++i, ++col) {
        double value1 = 0;
        double value2 = 0;
        if(i == polarChartAngularAxis.size() - 1) {
            value1 = ui->tableWidget->item(realIndex1, col - i)->text().toInt();
            value2 = ui->tableWidget->item(realIndex2, col - i)->text().toInt();
        }
        else {
            value1 = ui->tableWidget->item(realIndex1, col)->text().toInt();
            value2 = ui->tableWidget->item(realIndex2, col)->text().toInt();
        }
        int max_val = std::max(value1, value2);
        max_tot = std::max(max_tot, max_val);
        *series1 << QPointF(polarChartAngularAxis[i], value1);
        *series2 << QPointF(polarChartAngularAxis[i], value2);
    }

    series1->setName(ui->tableWidget->item(realIndex1, 2)->text());
    series2->setName(ui->tableWidget->item(realIndex2, 2)->text());

    radialAxis->setRange(0, max_tot);

    ui->chartView->setRenderHint(QPainter::Antialiasing);
    ui->chartView->setChart(chart);
}

void MainWindow::loadData() {
    // calling repository and get data
    repository* repo = repository::GetInstance();
    QList<QStringList> rowsData = repo->getData();
    // headers init
    ui->tableWidget->setColumnCount(rowsData[0].size());
    ui->tableWidget->setHorizontalHeaderLabels(rowsData[0]);
    ui->tableWidget->setRowCount(rowsData.size() - 1);
    ui->tableWidget->hideColumn(0);
    ui->tableWidget->hideColumn(1);
    rowsData.pop_front();
    for(int i = 0, c = 0; i < rowsData.size(); ++i) {
        for(int j = 0; j < rowsData[i].size(); ++j) {
            if( j == 0) {
                ui->tableWidget->setItem(i, 0, new QTableWidgetItem(rowsData[i][j].simplified()));
            }
            else if( j == 1) {
                QTableWidgetItem *item= new QTableWidgetItem();
                item->setData(Qt::EditRole, c++);
                ui->tableWidget->setItem(i, j, item);
            }
            else if(j == 2) {
                QString str = rowsData[i][j].simplified();
                ui->selectionBar1->addItem(str);
                ui->selectionBar2->addItem(str);
                QTableWidgetItem *item= new QTableWidgetItem();
                item->setData(Qt::EditRole, rowsData[i][j].simplified());
                item->setData(Qt::ForegroundRole, QVariant(QColor(Qt::blue)));
                ui->tableWidget->setItem(i, j, item);

            }
            else if(j == 3) {
                QStringList types = rowsData[i][j].simplified().split('-');
                MyTableWidgetItem *item = new MyTableWidgetItem(ui->tableWidget, types);
                ui->tableWidget->setCellWidget(i, j, item);
                ui->tableWidget->setItem(i, j, item);
            }
            else {
                QTableWidgetItem *item= new QTableWidgetItem();
                int value = rowsData[i][j].simplified().toInt();
                item->setData(Qt::EditRole, value);
                ui->tableWidget->setItem(i, j, item);
            }
        }
    }
}
