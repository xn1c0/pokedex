#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QListWidget>
#include <QCheckBox>

// Widget used to display charts
#include <QtCharts/QChartView>
// Used to make Pie Charts
#include <QtCharts/QPolarChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>

#include "repository.h"
#include "typewidget.h"
#include "mytablewidgetitem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    /**
     * @brief Clears all filter
     */
    void clearFiltersButtonClicked();
    /**
     * @brief Enables all filter
     */
    void enableFiltersButtonClicked();
    /**
     * @brief Called when a filter is changed
     */
    void filterCBStateChanged(const int state);
    /**
     * @brief Called when a cell of tablewidget is clicked
     * If the cell is on column name triggers chenge selectionbar1
     * index and transit to compare widnow
     */
    void myCellClicked(const int row, const int column);
    /**
     * @brief Transit to home view
     */
    void navigateToHomeButtonClicked();
    /**
     * @brief Called when selectionbar1 index is changed
     * Performs a series of operations.
     * Updates comboboxes
     * Updates image for left pokemon
     * Triggers chart update
     */
    void selectionBar1CurrentIndexChanged(const int index);
    /**
     * @brief Called when selectionbar2 index is changed
     * Performs a series of operations.
     * Updates comboboxes
     * Updates image for right pokemon
     * Triggers chart update
     */
    void selectionBar2CurrentIndexChanged(const int index);

private:
    Ui::MainWindow *ui; ///< ui
    QList<QCheckBox*> fCheckBoxes; ///< all type checkboxes
    int lastSelectionBar1Index; ///< last index of combobox1
    int lastSelectionBar2Index; ///< last index of combobox2

    QValueAxis *radialAxis; ///< chart radial axis
    QCategoryAxis *angularAxis; ///< chart angular axis
    QLineSeries *series1; ///< left pokemon series
    QLineSeries *series2; ///< right pokemon series
    QPolarChart *chart; ///< the polar chart

    /**
     * @brief Called upon filterCBStateChanged
     * Updates the tablewidget to hide/show rows based on match of type
     */
    void filterTable(const QString &filter, const bool state);
    /**
     * @brief Called by constructor
     * Loads table and comboboxes
     * This is for make table as a fake db to not call again a read on csv
     */
    void loadData();
    /**
     * @brief Called by selectionbar index changed
     * Updates chartview
     */
    void updateChart(const int barIndex1, const int barIndex2);
};
#endif // MAINWINDOW_H
